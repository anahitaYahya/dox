$(document).ready(function () {
    if ($('nav').width() > 991) {
        $('#navbar-search').click(function (e) {
            $(this).closest('nav').removeClass('searchHide').addClass('searchShow').children('.navChild').find('.searchContent-aside').show(500, function () {
                $(this).closest('nav').removeClass('searchShow').addClass('searchOpen');
            });
            $('#curtain').show();
            e.preventDefault();
        });
        $('.closeSearch').click(function (e) {
            $(this).closest('nav').removeClass('searchShow searchOpen').addClass('searchHide');
            $(this).closest('.searchContent-aside').hide();
            $('#curtain').hide();
            e.preventDefault();
        })

    }

    $('.navbar-toggler').click(function (e) {
        let firstSpan = $('span.navbar-close-icon:first-child');
        let secondSpan = $('span.navbar-close-icon:last-child');


       if (!$(this).hasClass('activeMenu')){
           $(this).addClass('activeMenu');
           $(this).find('span:last-child').css('top','0');
           $(this).find('span:first-child').css('top','0');
           setTimeout(()=>{
               firstSpan.addClass("firstRotate");
               secondSpan.addClass("secondRotate");
           },150);
           $('.navbar-collapse').fadeIn(200);
       }
       else {
           firstSpan.removeClass("firstRotate");
           secondSpan.removeClass("secondRotate");
           setTimeout(()=>{
               $(this).find('span:first-child').css('top','-5px');
               $(this).find('span:last-child').css('top','5px');
           },160);
           $(this).removeClass('activeMenu');
           $('.navbar-collapse').fadeOut(200);
       }
        e.stopPropagation();
    });
});

document.querySelector('.support-header-search').addEventListener('click', function (e) {
    e.stopPropagation();
    document.querySelector('.support-header-search').style.backgroundColor = '#fff';
    document.querySelector('.support-header-search-list').style.backgroundColor = '#fff';
    document.querySelector('.support-header-searchForm-textInput').style.borderColor = 'red';
    // document.querySelector('.support-header-search-list').style.display = 'block';
    document.querySelector('.support-header-search-list').style.visibility = 'visible';
    document.querySelector('.support-header-search-list').style.opacity = '1';
    document.querySelector('.support-header-search-list').style.transition = 'visibility .3s';
    document.querySelector('.support-header-search-list').style.transition = 'opacity .3s';
});

document.querySelector('.support-header-search-list').addEventListener('click', function (e) {
    e.stopPropagation();
});

document.querySelector('#support').addEventListener('click', function () {
    document.querySelector('.support-header-search').style.backgroundColor = 'transparent';
    document.querySelector('.support-header-search-list').style.backgroundColor = 'transparent';
    document.querySelector('.support-header-searchForm-textInput').style.borderColor = '#aaaaaa';
    // document.querySelector('.support-header-search-list').style.display = 'none';
    document.querySelector('.support-header-search-list').style.visibility = 'hidden';
    document.querySelector('.support-header-search-list').style.opacity = '0';
    document.querySelector('.support-header-search-list').style.transition = 'visibility .3s';
    document.querySelector('.support-header-search-list').style.transition = 'opacity .3s';
});