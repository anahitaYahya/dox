<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dox Technologies</title>
    <link type="text/css" rel="stylesheet" href="{{'css/style.css'}}">
    {{--<link rel="stylesheet" href="{{'css/FeaturePhone.css'}}">--}}
    {{--<script src="{{ asset('js/jquery.js') }}" defer></script>--}}

    {{--<script src="{{asset('js/app.js')}}"></script>--}}
{{--    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>--}}
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--}}
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{ asset('js/function.js') }}" defer></script>
    <script src="{{ asset('js/imgToSvg.js') }}" defer></script>
</head>
<body>

    <div class="container-fluid p-0">
    <header class="header">
        <nav class="navbar navbar-expand-lg navbar-light justify-content-center">
            <div class="navChild h-100 d-flex justify-content-center position-relative">
                <a class="navbar-brand" href="http://localhost/Dox/public/">
                    <img class="svg doxLogo" src="svg/DoxLogoColor.svg" alt="doxLogo">
                </a>
                <div class="{{--collapse--}} navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item active navLi">
                            <a class="nav-link" href="http://localhost/Dox/public/feature">
                            <span>
                                Feature Phone
                            </span>
                            </a>
                        </li>
                        <li class="nav-item navLi">
                            <a class="nav-link" href="#">
                            <span>
                                Smart Phone
                            </span>
                            </a>
                        </li>
                        <li class="nav-item navLi">
                            <a class="nav-link" href="http://localhost/Dox/public/support" id="navbarDropdown">
                            <span>
                                Support
                            </span>
                            </a>
                        </li>
                        <li class="navLi" id="navbar-search">
                            <img src="{{asset('svg/Search.svg')}}" alt="search" class="svg search">
                        </li>
                    </ul>
                </div>
                <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-close-icon w-100"></span>
                    {{--<span class="navbar-close-icon w-100 invisible"></span>--}}
                    <span class="navbar-close-icon w-100"></span>
                </button>
                {{--<input type="checkbox">--}}
                <aside class="searchContent-aside">
                    <div class="searchContent position-absolute">
                        <div class="searchInput">
                            <form action="">
                                <div class="searchForm-slide">
                                    <label for="searchInput"></label>
                                    <input type="text" id="searchInput">
                                    <button class="searchForm-search">
                                        <img src="{{asset('svg/Search.svg')}}" alt="" class="svg search">
                                    </button>
                                </div>
                            </form>
                        </div>
                        <aside class="searchResults position-relative">
                            <section>
                                <div>
                                    <h6>QUICK LINKS</h6>
                                    <ul class="searchUl">
                                        <li><a href="">Us Warranty</a></li>
                                        <li><a href="">User Manual</a></li>
                                        <li><a href="">News and Events</a></li>
                                        <li><a href="http://localhost/Dox/public/touch">Contact Us</a></li>
                                    </ul>

                                </div>
                            </section>
                        </aside>
                    </div>
                    <button id="navbar-exitSearch" class="closeSearch">
                        <img src="{{asset('svg/Exit.svg')}}" alt="" class="svg exit">
                    </button>
                </aside>
            </div>
        </nav>
    </header>
    <div id="curtain"></div>
    <div class="main">
        @yield('content')
    </div>
    <footer class="footer">
        <div class="footerContent">
            <h4 class="mb-4">At Dox Technologies, our mission is to fulfill our customer desires.</h4>
            <p class="mb-4">DOX Technologies Inc, is a rapid growing mobile company, established in 2017 in the USA,
                backed by years
                of
                experience in IT industry, proud to serve customers throughout whole Middle East and North Africa the
                wide
                range
                of products including feature phones, smart phone and tablets. We are also proud to provide our
                customers
                the
                best mobile phones with economical prices and customer service after their purchase</p>
            <h5 class="pb-5">Everything we do is to have your satisfaction</h5>
            <p class="pt-5">2017 Dox Technologies INC. All Right Reserved </p>
            <p> Dox Technologies and the Stylized Dox Logo are Registered Trademarks of Dox Technologies, INC </p>
            <p class="pb-5 mb-4"> All Mobilephone are Designed by Dox Technologies in California Assembled in China</p>
            <div class="mb-3">
                <img src="svg/Flag.svg" alt="flag" class="svg flag">
            </div>
            <div>
                <img src="svg/DoxLogoGrey.svg" alt="doxLogo" class="svg doxLogo">
            </div>
            <div class="row justify-content-end largeFooter">
                <table class="table">
                    <thead>
                    <tr>
                        <td>Categorization</td>
                        <td>Support</td>
                        <td>About Dox</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="http://localhost/Dox/public/feature">Feature Phone</a></td>
                        <td><a href="http://localhost/Dox/public/faq">FAQ</a></td>
                        <td><a href="">News and Events</a></td>
                    </tr>
                    <tr>
                        <td><a href="">Smart Phone</a></td>
                        <td><a href="">Us Warranty</a></td>
                        <td><a href="">Q & A</a></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href="">User Manual</a></td>
                        <td><a href="">Contact us</a></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><a href="">Job Opportunity</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row smallFooter my-4">
                <div class="accordion" id="footerAccordion">
                    <div class="card">
                        <div class="card-header collapsed" id="footerHeadingOne" data-toggle="collapse"
                             data-target="#footerCollapseOne" aria-expanded="true" aria-controls="footerCollapseOne">
                            <h2 class="mb-0">
                                About Dox
                            </h2>
                        </div>

                        <div id="footerCollapseOne" class="collapse" aria-labelledby="footerHeadingOne"
                             data-parent="#footerAccordion">
                            <div class="card-body">
                                <a href="">
                                    News and Events
                                </a>
                                <a href="">
                                    Q & A
                                </a>
                                <a href="">
                                    Contact us
                                </a>
                                <a href="">
                                    Job Opportunity
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" id="footerHeadingTwo" data-toggle="collapse"
                             data-target="#footerCollapseTwo" aria-expanded="false" aria-controls="footerCollapseTwo">
                            <h2 class="mb-0">
                                Support
                            </h2>
                        </div>
                        <div id="footerCollapseTwo" class="collapse" aria-labelledby="footerHeadingTwo"
                             data-parent="#footerAccordion">
                            <div class="card-body">
                                <a href="#">
                                    FAQ
                                </a>
                                <a href="#">
                                    Us Warrantya
                                </a>
                                <a href="#">
                                    User Manual
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header collapsed" id="footerHeadingThree" data-toggle="collapse"
                             data-target="#footerCollapseThree" aria-expanded="false"
                             aria-controls="footerCollapseThree">
                            <h2 class="mb-0">
                                Categorization
                            </h2>
                        </div>
                        <div id="footerCollapseThree" class="collapse" aria-labelledby="footerHeadingThree"
                             data-parent="#footerAccordion">
                            <div class="card-body">
                                <a href="http://localhost/Dox/public/feature">
                                    Feature Phone
                                </a>
                                <a href="#">
                                    Smart Phone
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="socialMedia">
            <span class="socialIcons"><img src="svg/Twitter.svg" alt="Twitter" class="svg socialIconsSvg"></span>
            <span class="socialIcons"><img src="svg/Instagram.svg" alt="Instagram" class="svg socialIconsSvg"></span>
            <span class="socialIcons"><img src="svg/Facebook.svg" alt="Facebook" class="svg socialIconsSvg"></span>
            <span class="socialIcons"><img src="svg/Google.svg" alt="Google" class="svg socialIconsSvg"></span>
            <span class="socialIcons"><img src="svg/Youtube.svg" alt="Youtube" class="svg socialIconsSvg"></span>
        </div>
    </footer>
</div>
</body>
</html>
