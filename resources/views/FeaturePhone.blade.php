@extends('layout')
@section('content')

        <section class="DocxSlider">
            <div id="carouselIndicators" class="carousel slide" data-ride="carousel" data-type="multi">
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                       <img src="{{ asset('svg/B100 White.svg') }}" class="svg First B100_1" />
                   </div>
                    <div  class="item">
                       <img src="{{ asset('svg/B120 White.svg') }}" class="svg Second B110_1" alt="" />
                    </div>
                    <div class="item">
                       <img src="{{ asset('svg/B430 White.svg') }}" class="svg Third B120_1" alt="">
                     </div>
                </div>

                               <span class="carousel-control-prev LeftBtn" aria-hidden="true" href="#carouselExampleIndicators" role="button" data-slide="prev">

                               </span>


                               <span class="carousel-control-next RightBtn" aria-hidden="true" href="#carouselExampleIndicators" role="button" data-slide="next">


                               </span>

            </div>

        </section>
        <!-- Need nav btn -->




        <section class="UltimateView row">
                {{--  imgHere  --}}
            <h1>Ultimate View</h1>
        </section>
        <section class="FeaturePhone row">
                {{--  FeaturePhone  --}}

                 <span  class="priceIcon"></span>

                  <span class=" FeatureDetail GreyText col-sm-8 col-md-10 ">Feature Phone &gt <strong class="blackText col-md-2">B400</strong> <span  class="price col-sm-4 col-md-3 col-lg-3">$ 8.00 </span></span>

        </section>
        <section class="DocxDetails col-12 row ">
            <div class="Rightclm  col-sm-12  ">
                       <div class="Box">
                            <span class="BoxText">In the box</span>
                            <span class="BoxText">DOX B400</span>
                            <span class="BoxText">Quick start guide</span>
                            <span class="BoxText">Micro USB charger</span>
                            <span class="BoxText">Charging data cable</span>
                       </div>

                        <div class="Performance">
                            <span>Performance</span>
                            <span>RAM: 32 MB </span>
                            <span>Storage: 64 MB</span>
                        </div>

                        <div class="Design">
                            <span class="h1">Design</span>
                            <span>Colors: Black, White</span>
                            <span>Size: 112.5 x 50.5 x 12.9mm </span>
                            <span>Weight: 94.0 gr (including Battery)</span>
                        </div>

                        <div class="Battry">
                            <span>Battery</span>
                            <span>Type: 800 mAh </span>
                            <span>Removable Yes</span>
                        </div>

                        <div class="Display">
                            <span>Display</span>
                            <span>Size: 2.44" </span>
                            <span>Type: QVGA TN </span>
                            <span>Resolution: 240 x 320</span>
                        </div>

                        <div class="Camera">
                            <span>Camera</span>
                            <span>Rear: 0.08MP</span>
                        </div>

                        <div class="Networks">
                            <span>Networks</span>
                            <span>GSM:</span>
                            <span>GSM (US): 850/1900 </span>
                            <span>GSM (EU): 900/1800 </span>
                            <span>Charging connector: Micro USB</span>
                        </div>
            </div>
            <div class="Leftclm col-sm-12 ">
                <div class="StraightPitureBig col-sm-12">
                    <img src="{{ asset('images/B100_700X350/B100_Black_F.png') }}" class="svg First B100_1" />
                </div>
{{--                <div class="clearfix"></div>--}}

                <div class="Sidewayright col-sm-12 ">
                    <img src="{{ asset('images/B100_700X350/B100_Black_S.png') }}" class="svg First B100_1" />
                    <img src="{{ asset('images/B100_700X350/B100_Black_B.png') }}" class="svg First B100_1" />
                    <img src="{{ asset('images/B100_700X350/B100_Black_S.png') }}" class="svg First B100_1" />

                </div>
                <div class="ButtonHolder col-sm-12 col-md-12">
                    <input class="styled-checkbox-white" id="styled-checkbox-white" type="checkbox" value="white">
                    <label for="styled-checkbox-white"></label>
                    <input class="styled-checkbox-black" id="styled-checkbox-black" type="checkbox" value="black">
                    <label for="styled-checkbox-black"></label>
                </div>

                <div class="DetailSvg row col-sm-12 col-md-12">
                    <div class="column">
                        <i class="col-sm-2 col-md-2 simcard"></i>
                        <span class="text"> <strong class="strong">Sim Card </strong> <br> <em class="em">Dual-SIM</em></span>
                    </div>
                    <div class="column">
                        <i class="col-sm-2 col-md-2 display"></i>
                        <span class="text">  <strong class="strong">Display </strong>  <br><em class="em">2.44" </em> </span>
                    </div>
                    <div class="column">
                        <i class="col-sm-2 col-md-2 camera"></i>
                        <span class="text"> <strong class="strong">0.08MP</strong> <br><em class="em"> Camera </em></span>
                    </div>
                    <div class="column">
                        <i class="col-sm-2 col-md-2 Memory"></i>
                        <span class="text"><strong class="strong">64 MB</strong> <br> <em class="em">Memory </em></span>
                    </div>
                    <div class="column">
                        <i class="col-sm-2 col-md-2 radio"></i>
                        <span class="text"><strong class="strong">Radio  </strong><br>  <em class="em">Built in FM Antena, FM Radio</em></span>
                    </div>
                    <div class="column">
                        <i class="col-sm-2 col-md-2 battery"></i>
                        <span class="text"><strong class="strong">Battery  </strong> <br><em class="em"> 800 mAh</em></span>
                    </div>
                </div>

            </div>

        </section>
        <section class="UserGuid row">
            <span class="GuidBg col-md-12 ">
                   <img src="{{ asset('svg/User Guides.svg') }}" class="svg First B100_1" />
            </span>
            <span class="GuideText col-md-12 ">User Guides</span>
            <button class="btn btn-link col-md-12">Click here ></button>
        </section>
        <section class="RelatedProducs col-12 row">
            <h2 class="font-weight-bold csm-header"> Related products</h2>
            <div class="col-sm-4 col-md-4 col-lg-4 column pt-5">
                <span class="GuidBg col-md-12 ">
                            <img src="{{ asset('svg/B110_1.svg') }}" class="svg First B100_1" />
                </span>
                <span class="GuideText col-md-4 "> B100 </span>
                <span class="GuideText col-md-4 ">$8.00</span>
                <button class="btn btn-link col-md-4">Click here ></button>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 column pt-5 pb-5">
                <span class="GuidBg col-md-12 ">
                    <img src="{{ asset('svg/B120_1.svg') }}" class="svg First B100_1" />
                </span>
                <span class="GuideText col-md-12 "> B110</span>
                <span class="GuideText col-md-12 ">$10.00</span>
                <button class="btn btn-link col-md-12">Click here ></button>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4 column pt-5 pb-5">
                <span class="GuidBg col-md-12 ">
                     <img src="{{ asset('svg/B120_1.svg') }}" class="svg First B100_1" />
                </span>
                <span class="GuideText col-md-12 ">B430</span>
                <span class="GuideText col-md-12 ">$8.00</span>
                <button class="btn btn-link col-md-12">Click here ></button>
            </div>
        </section>


    <script>
        // $('#carouselIndicators').on('slide.bs.carousel', function () {
        //     // do something...
        // })
        $('#carouselIndicators').carousel({
            interval:   false,

        });
    </script>
@endsection
