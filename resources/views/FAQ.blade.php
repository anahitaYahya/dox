@extends('layout')
@section('content')
    <div id="FAQ">
        <div class="row picture d-flex align-items-center justify-content-center" style="background: white url('{{asset("images/18.jpg")}}')">
            <div class="d-flex flex-column text-white position-relative">
                <h1 class="text-center font-weight-bold">FAQ</h1>
                <h2 class="text-center font-weight-light">Need help? We’ve got you covered</h2>
            </div>
        </div>
        <div class="row">
            <div class="FAQtitle w-100 text-center">
                <h2 class="font-weight-bold">Frequently Asked Questions</h2>
                <h2 class="font-weight-bold" class="mb-5">(FAQ)</h2>
                <img src="svg/Answer.svg" alt="answers" class="svg mt-2">
                <h3>Answers</h3>
                <h5 class="font-weight-normal">Perhaps, you can find your</h5>
                <h5 class="font-weight-normal">answer here!</h5>
            </div>
            <div class="container AllCollapses">
                <div id="FAQaccordion" class="accordion bg-white px-5">
                    <div class="card mb-0 border-top-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-target="#collapse1"
                             aria-expanded="false" aria-controls="collapse1">
                            <a class="card-title">
                                I lost my cell phone. What do I do?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse1" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">
                                I lost my cell phone. What do I do?First, immediately contact your provider and suspend
                                your cell number to prevent it from being used
                                fraudulently. You then have a few options:
                                Wait to see if the lost phone turns up.
                                Order a new phone to replace the lost one.
                                Permanently disconnect your cellular number if it's not on a contract.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to suspend
                                the phone.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQaccordion"
                             data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            <a class="card-title">
                                What do I do if my cell phone isn't working properly?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse2" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQaccordion"
                             data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                            <a class="card-title">
                                How do I get my cell phone repaired?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse3" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQaccordion"
                             data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                            <a class="card-title">
                                How do I disconnect my cell phone and what do I do with the phone?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse4" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQaccordion"
                             data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                            <a class="card-title">
                                Can I upgrade my phone and keep the same number?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse5" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                    <div class="card mb-0 border-bottom-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#FAQaccordion"
                             data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                            <a class="card-title">
                                Can I upgrade my phone and keep the same number?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapse6" class="collapse" data-parent="#FAQaccordion">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row flex-column justify-content-center align-items-center bg-white">
            <h2 class="mb-5 font-weight-bold">Still have questions that have not been answered?</h2>
            <img src="{{asset('svg/contact.svg')}}" alt="" class="svg">
            <h4 class="{{--font-weight-light--}}">Contact us and use our advice</h4>
            <h6 class="{{--font-weight-light--}}">Contact us on email and we'll reply with</h6>
            <h6 class="{{--font-weight-light--}}">an answer to your question or issue.</h6>
            <div class="btn btn-outline-primary p-2">
                <a href="#">Contact Us</a>
            </div>
        </div>
    </div>
@endsection
