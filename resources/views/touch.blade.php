@extends('layout')
@section('content')
    <div id="touch">
        <div class="bannerTouch" style="background: url('{{asset("images/16.jpg")}}')">
            <div class="bannerTitle"><span>Get in touch</span></div>
            <div class="bannerText"><p>Want to get in touch?<br>We’d love to hear from you. Here’s how you can reach us…</p></div>
        </div>
        <div class="row">
                <div class="col-md-12">
                    <div class="ContactUs"><h2>Contact us</h2></div>
                    <div class="col-md-12 Contact">
                        <div class="col-md-4">
                            <div class="ContactOptions">
                                <div class="ContactSvg">
                                    <img src="svg/Address.svg" alt="Address" class="svg">
                                </div>
                                <div class="svgName"><h5>Email</h5></div>
                                <div class="ContactDescriptions">
                                    <p>Our team is happy to answer<br>your questions<br>Fill out the form and we’ll be in<br>touch as soon as possible.</p>
                                </div>
                                <div class="contactLink"><a href="">info@doxtechnologies.com</a></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="ContactOptions">
                                <div class="AddressSvg">
                                    <img src="svg/Address.svg" alt="Address" class="svg">
                                </div>
                                <div class="svgName"><h5>Address</h5></div>
                                <div class="ContactDescriptions">
                                    <p>Meet us in real world<br>5530 Corbin Ave., #102,<br>Tarzana CA 91356, USA</p>
                                </div>
                                <div class="contactLink"><a href="">(Direction)</a></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="ContactOptions">
                                <div class="ContactSvg">
                                    <img src="svg/Address.svg" alt="Address" class="svg">
                                </div>
                                <div class="svgName"><h5>Phone number</h5></div>
                                <div class="ContactDescriptions">
                                    <p>Interested in DOX’s phone or<br>any other inquires? Just pick up <br>the phone to talk with a <br>member of our team.</p>
                                </div>
                                <div class="contactLink"><a href="">+1 626 319 74 14</a></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <div class="callCenter">
            <div class="col-md-12 d-flex">
                <div class="col-md-6 callCenterImg" style="background: url('{{asset("images/Capture.PNG")}}')">
                </div>
                <div class="col-md-6 callCenterText">
                    <div class="Line1">
                        <span>Our</span>
                        <span style="color: #EE1D23">team</span>
                        <span>is happy</span>
                    </div>
                    <div class="Line2">
                        <span>to</span>
                        <span style="color: #EE1D23">answer</span>
                        <span>your</span>
                        <span style="color: #EE1D23">questions</span><span>.</span>
                    </div>
                    <br>
                    <br>
                    <div class="Line3">
                        <span>Please</span>
                        <span style="color: #EE1D23">feel</span>
                        <span style="color: #EE1D23">free</span>
                        <span>to</span>
                        <span>send</span>
                        <span>us</span>
                    </div>
                    <div class="line4">
                        <span>email or select from our other </span>
                    </div>
                    <div class="line5">
                        <span>options to</span>
                        <span style="color: #EE1D23">contact us</span><span>.</span>
                    </div>
                    <p>Sometimes you need a little help from <br> friends. Or a DOX Technologies <br> rep. Don’t worry… we’re here<br>for you.</p>


                </div>
            </div>
        </div>
        <div class="OnlineHelp">
            <h3 class="titleHelp">Online help</h3>
            <div class="ContactSvg">
                <img src="svg/ContactForm.svg" alt="Topics" class="svg">
            </div>
            <div class="svgHeader">Contact form</div>
            <div class="textContainer">
                <p>Fill out the form and we’ll be <br> touch as soon as possible.</p>
            </div>
        </div>
        <div class="ContactForm">
            <form>
                <input class="form-control form-control-lg" type="text" placeholder="Contact Us">
                <input type="email" class="form-control form-control-lg" id="emailAddress" placeholder="Your email address">
                <input class="form-control form-control-lg" type="text" placeholder="Subject">
                <textarea class="form-control" id="Textarea1" rows="3" placeholder="Your message"></textarea>
                <div class="submitBtn">
                    <button type="submit" class="btn">Submit</button>
                </div>
            </form>
        </div>
        <div class="location">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d207371.9779494947!2d51.209735416674455!3d35.69701178455405!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e00491ff3dcd9%3A0xf0b3697c567024bc!2sTehran%2C%20Tehran%20Province%2C%20Iran!5e0!3m2!1sen!2s!4v1576327292343!5m2!1sen!2s" width="900" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>

    </div>
@endsection