<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homePage');
});
Route::get('/layout', function () {
    return view('layout');
});
Route::get('/home', function () {
    return view('homePage');
});
Route::get('/faq', function () {
    return view('FAQ');
});
Route::get('/populartopics', function () {
        return view('popularTopics');
});
Route::get('/feature', function () {
    return view('FeaturePhone');
});
Route::get('/touch', function () {
    return view('touch');
});

Route::get('/support', function () {
    return view('support');
});

Route::get('/maplocation', function () {
    return view('mapLocation');
});