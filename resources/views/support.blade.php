
@extends('layout')
@section('content')
    <section id="support">
        <section class="support-header" style="background: linear-gradient(180deg ,rgba(255, 255, 255,1),transparent, transparent), url('{{asset('images/19.jpg')}}') 67% 0/cover">
            <h1 class="support-header-h1">
                <span class="support-header-welcomeSupport">Welcome&nbsp;to&nbsp;support</span>
                <span class="support-header-doxTechnology">Dox&nbsp;Technology</span> 
            </h1>
            <div class="support-header-search">
                <form action="" class="support-header-searchForm">
                    <div class="support-header-searchForm-container">
                        <button class="support-header-searchForm-button" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="support-header-searchForm-container">
                        <input type="text" name="" id="" placeholder="Search" class="support-header-searchForm-textInput">
                    </div>
                </form>
            </div>
            <ul class="support-header-search-list">
                <li>Popular Topics</li>
                <li>
                    <a href="#" class="support-header-search-list-links">Why does my phone occasionally stop charging before it reaches 100%?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">My phone won’t charge. What should I do?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">I recently updated my Dox and am now having some issues with the Camera.</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">How can I resolve this?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">How can I close an app that has stopped working?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Where can I find the IMEI number for my phone?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Can I remove the back cover or battery from my Dox B100?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Where can I get support for my Dox smartphone?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Why does my phone feel warm when it's charging or when I'm using it</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Why can’t I turn off the camera shutter sound?</a>
                </li>
                <li>
                    <a href="#" class="support-header-search-list-links">Will my Dox smartphone receive security updates?</a>
                </li>
            </ul>
        </section>
        <section class="findAnswer">
            <h2 class="findAnswer-header">Find&nbsp;Answers</h2>
            <div class="findAnswer-customer">
                <div class="findAnswer-customer-email">
                    <div class="findAnswer-customer-email-svg">
                        <!-- <img class="email-svg" src="img/Email.svg" alt=""> -->
                        <svg class="email-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="0.710528in" height="0.544374in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 2.42 1.86">
                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer"/>
                                <path class="supportSvg" d="M2.3 1.78l-2.19 0c-0.02,0 -0.04,-0.02 -0.04,-0.04l0 -1.62c0,-0.02 0.02,-0.04 0.04,-0.04l2.19 0c0.02,0 0.04,0.02 0.04,0.04l0 1.62c0,0.02 -0.02,0.04 -0.04,0.04zm0 -1.78l-2.19 0c-0.07,0 -0.12,0.05 -0.12,0.12l0 1.62c0,0.07 0.05,0.12 0.12,0.12l2.19 0c0.07,0 0.12,-0.05 0.12,-0.12l0 -1.62c0,-0.07 -0.05,-0.12 -0.12,-0.12z"/>
                                <path class="supportSvg" d="M2.25 0.18c-0.01,-0.01 -0.02,-0.01 -0.03,-0.01 -0.01,-0 -0.02,0 -0.03,0.01l-0.93 0.78c-0.03,0.03 -0.08,0.03 -0.11,0l-0.93 -0.78c-0.01,-0.01 -0.02,-0.01 -0.04,-0.01 -0.01,0 -0.02,0.02 -0.02,0.03 -0,0.01 0,0.03 0.01,0.04l0.93 0.78c0.03,0.02 0.07,0.04 0.1,0.04 0.04,0 0.07,-0.01 0.1,-0.04l0.93 -0.78c0.01,-0.01 0.01,-0.02 0.01,-0.03 0,-0.01 -0,-0.02 -0.01,-0.03z"/>
                                <path class="supportSvg" d="M0.8 1.04c-0,-0.01 -0.01,-0.02 -0.03,-0.03 -0.01,-0 -0.03,0 -0.04,0.01l-0.57 0.61c-0.01,0.01 -0.01,0.02 -0.01,0.04 0,0.01 0.01,0.02 0.03,0.03 0,0 0.01,0 0.01,0 0.01,0 0.02,-0 0.03,-0.01l0.57 -0.61c0.01,-0.01 0.01,-0.02 0.01,-0.04z"/>
                                <path class="supportSvg" d="M2.25 1.63l-0.57 -0.61c-0.01,-0.01 -0.02,-0.01 -0.03,-0.01 -0,0 -0.01,0 -0.01,0 -0.01,0 -0.02,0.01 -0.03,0.03 -0,0.01 -0,0.03 0.01,0.04l0.57 0.61c0.01,0.01 0.02,0.01 0.03,0.01 0.01,0 0.02,-0 0.03,-0.01 0.01,-0.01 0.02,-0.04 0,-0.05z"/>
                            </g>
                        </svg>
                    </div>
                    <h3 class="findAnswer-customer-header">
                        Send&nbsp;us&nbsp;an&nbsp;email
                    </h3>
                    <p class="findAnswer-customer-text">
                        Contact us on email and we'll
                    </p>
                    <p class="findAnswer-customer-text">
                        reply with an answer to your
                    </p>
                    <p class="findAnswer-customer-text">
                        question or issue.
                    </p>
                    <a class="findAnswer-customer-link" href="#">Click here 	&#62;</a>
                </div>
                <div class="findAnswer-customer-contact">
                    <div class="findAnswer-customer-contact-svg">
                        <!-- <img class="contact-svg" src="img/Talk.svg" alt=""> -->
                        <svg class="contact-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="0.666717in" height="0.666669in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 2.2 2.2">

                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer"/>
                                <path class="supportSvg" d="M0.09 0.31c0.01,-0.05 0.04,-0.09 0.09,-0.11l0.19 -0.11c0.03,-0.02 0.06,-0.01 0.08,0.02l0.16 0.24 0.14 0.2c0.02,0.02 0.01,0.06 -0.01,0.08l-0.17 0.13c-0.05,0.04 -0.07,0.11 -0.03,0.16l0.01 0.02c0.05,0.09 0.11,0.19 0.32,0.41 0.21,0.21 0.32,0.27 0.41,0.32l0.02 0.01c0.06,0.03 0.13,0.02 0.16,-0.03l0.13 -0.17c0.02,-0.02 0.05,-0.03 0.08,-0.01l0.45 0.3c0.03,0.02 0.03,0.05 0.02,0.08l-0.11 0.19c-0.02,0.04 -0.06,0.07 -0.11,0.09 -0.23,0.06 -0.62,0.01 -1.22,-0.59 -0.6,-0.6 -0.65,-0.99 -0.59,-1.22zm1.65 1.9c0.06,0 0.12,-0.01 0.17,-0.02 0.07,-0.02 0.12,-0.06 0.16,-0.12l0.11 -0.19c0.04,-0.06 0.02,-0.14 -0.04,-0.17l-0.45 -0.3c-0.06,-0.04 -0.13,-0.03 -0.17,0.03l-0.13 0.17c-0.02,0.02 -0.05,0.03 -0.07,0.01l-0.02 -0.01c-0.08,-0.04 -0.18,-0.1 -0.39,-0.31 -0.21,-0.21 -0.26,-0.31 -0.31,-0.39l-0.01 -0.02c-0.01,-0.02 -0.01,-0.05 0.01,-0.07l0.17 -0.13c0.05,-0.04 0.07,-0.12 0.03,-0.17l-0.3 -0.45c-0.04,-0.06 -0.12,-0.07 -0.17,-0.04l-0.19 0.11c-0.06,0.03 -0.1,0.09 -0.12,0.16 -0.07,0.24 -0.02,0.67 0.6,1.29 0.49,0.49 0.86,0.63 1.12,0.63z"/>
                                <path class="supportSvg" d="M1.25 0.37c0.34,0 0.62,0.28 0.62,0.62 0,0.02 0.02,0.04 0.04,0.04 0.02,0 0.04,-0.02 0.04,-0.04 -0,-0.38 -0.31,-0.7 -0.7,-0.7 -0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04z"/>
                                <path class="supportSvg" d="M1.25 0.59c0.22,0 0.4,0.18 0.4,0.4 0,0.02 0.02,0.04 0.04,0.04 0.02,0 0.04,-0.02 0.04,-0.04 -0,-0.26 -0.21,-0.48 -0.48,-0.48 -0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04z"/>
                                <path class="supportSvg" d="M1.25 0.81c0.1,0 0.18,0.08 0.18,0.18 0,0.02 0.02,0.04 0.04,0.04 0.02,0 0.04,-0.02 0.04,-0.04 -0,-0.14 -0.12,-0.26 -0.26,-0.26 -0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04z"/>
                            </g>
                        </svg>
                    </div>
                    <h3 class="findAnswer-customer-header">
                        Talk&nbsp;to&nbsp;us
                    </h3>
                    <p class="findAnswer-customer-text">
                        Contact us online and get
                    </p>
                    <p class="findAnswer-customer-text">
                        support from an expert on
                    </p>
                    <p class="findAnswer-customer-text">
                        your mobile.
                    </p>
                    <a class="findAnswer-customer-link" href="#">Click here 	&#62;</a>
                </div>
                <div class="findAnswer-customer-chat">
                    <div class="findAnswer-customer-chat-svg">
                        <!-- <img class="chat-svg" src="img/Chat.svg" alt=""> -->
                        <svg class="chat-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="0.726154in" height="0.66637in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 2.69 2.47">

                            <g id="Layer_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer"/>
                                <path class="supportSvg" d="M1.03 0.95l-0.59 0c-0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04l0.59 0c0.02,0 0.04,-0.02 0.04,-0.04 0,-0.02 -0.02,-0.04 -0.04,-0.04z"/>
                                <path class="supportSvg" d="M1.62 1.22l-1.17 0c-0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04l1.17 0c0.02,0 0.04,-0.02 0.04,-0.04 0,-0.02 -0.02,-0.04 -0.04,-0.04z"/>
                                <path class="supportSvg" d="M1.62 1.49l-1.17 0c-0.02,0 -0.04,0.02 -0.04,0.04 0,0.02 0.02,0.04 0.04,0.04l1.17 0c0.02,0 0.04,-0.02 0.04,-0.04 0,-0.02 -0.02,-0.04 -0.04,-0.04z"/>
                                <path class="supportSvg" d="M0.26 1.89c-0.1,0 -0.18,-0.08 -0.18,-0.18l0 -0.95c0,-0.1 0.08,-0.18 0.18,-0.18l1.54 -0c0.1,0 0.18,0.08 0.18,0.18l0 0.95c0,0.1 -0.08,0.18 -0.18,0.18l-0.77 0c-0.01,0 -0.02,0 -0.03,0.01l-0.38 0.42 0 -0.39c0,-0.02 -0.02,-0.04 -0.04,-0.04l-0.32 0zm0.45 -1.63c0,-0.1 0.08,-0.18 0.18,-0.18l1.54 0c0.1,0 0.18,0.08 0.18,0.18l-0 0.96c-0,0.1 -0.08,0.18 -0.18,0.18l-0.09 0c-0.02,0 -0.04,0.02 -0.04,0.04l0 0.39 -0.01 -0.01 -0.23 -0.25 0 -0.81c0,-0.15 -0.12,-0.26 -0.26,-0.26l-1.09 0 0 -0.23zm-0.15 2.21c0.02,0.01 0.03,0 0.05,-0.01l0.44 -0.48 0.75 -0c0.15,0 0.26,-0.12 0.26,-0.26l0 -0.02 0.01 0.01 0.24 0.26c0.01,0.01 0.03,0.02 0.05,0.01 0.02,-0.01 0.03,-0.02 0.03,-0.04l0 -0.45 0.05 0c0.15,0 0.26,-0.12 0.26,-0.26l0 -0.95c0,-0.14 -0.12,-0.26 -0.26,-0.26l-1.54 -0c-0.15,0 -0.26,0.12 -0.26,0.26l0 0.23 -0.37 0c-0.15,0 -0.26,0.12 -0.26,0.26l0 0.95c0,0.15 0.12,0.26 0.26,0.26l0.28 0 0 0.45c0,0.02 0.01,0.03 0.03,0.04z"/>
                            </g>
                        </svg>
                    </div>
                    <h3 class="findAnswer-customer-header">
                        Chat&nbsp;with&nbsp;us
                    </h3>
                    <p class="findAnswer-customer-text">
                        Live Chat support with a
                    </p>
                    <p class="findAnswer-customer-text">
                        Dox Technology
                    </p>
                    <p class="findAnswer-customer-text">
                        Care Pro.
                    </p>
                    <a class="findAnswer-customer-link" href="#">Click here 	&#62;</a>
                </div>
            </div>
        </section>
        <section class="service">
            <h2 class="service-header">
                Service and Repair
            </h2>
            <div class="service-content">
                <div class="service-content-mainContent">
                    <div class="service-content-mainContent-svg">
                        <!-- <img class="service-svg" src="img/Service.svg" alt=""> -->
                        <svg class="service-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="0.666654in" height="0.666673in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 2.2 2.2">

                                <g id="Layer_x0020_1">
                                    <metadata id="CorelCorpID_0Corel-Layer"/>
                                    <path class="supportSvg" d="M0.53 1.91l-0.18 0 -0.09 -0.15 0.09 -0.15 0.18 0 0.09 0.15 -0.09 0.15zm0.05 -0.35c-0.01,-0.01 -0.02,-0.02 -0.03,-0.02l-0.22 0c-0.01,0 -0.02,0.01 -0.03,0.02l-0.11 0.18c-0.01,0.01 -0.01,0.03 0,0.04l0.11 0.18c0.01,0.01 0.02,0.02 0.03,0.02l0.22 0c0.01,0 0.02,-0.01 0.03,-0.02l0.11 -0.18c0.01,-0.01 0.01,-0.03 0,-0.04l-0.11 -0.18z"/>
                                    <polygon class="supportSvg" points="1.53,0.72 0.72,1.53 0.67,1.48 1.48,0.67 "/>
                                    <polygon class="supportSvg" points="1.9,1.84 1.84,1.9 1.48,1.53 1.53,1.48 "/>
                                    <path class="supportSvg" d="M1.98 2.11c-0.03,0.01 -0.05,0.01 -0.08,0.01 -0.03,-0 -0.05,-0 -0.08,-0.01l0.29 -0.29c0.04,0.11 -0.01,0.24 -0.13,0.29zm-0.24 -0.05l-0.41 -0.41c0.04,-0.05 0.03,-0.12 -0.01,-0.16 -0.04,-0.04 -0.1,-0.04 -0.15,0l-0.06 -0.06 0.32 -0.31 0.06 0.06c-0.04,0.05 -0.03,0.12 0.01,0.16 0.04,0.04 0.1,0.04 0.15,-0l0.41 0.41c0,0 0.01,0.01 0.01,0.01l-0.31 0.31c-0,-0 -0.01,-0.01 -0.01,-0.01zm-0.94 -0.42c-0.01,0.01 -0.01,0.02 -0.01,0.03 0.05,0.2 -0.07,0.4 -0.27,0.45 -0.2,0.05 -0.4,-0.07 -0.45,-0.27 -0.05,-0.2 0.07,-0.4 0.27,-0.45 0.06,-0.02 0.12,-0.02 0.18,0 0.01,0 0.03,-0 0.03,-0.01l0.83 -0.83c0.01,-0.01 0.01,-0.02 0.01,-0.03 -0.05,-0.2 0.07,-0.39 0.26,-0.44 0.04,-0.01 0.08,-0.01 0.11,-0.01l-0.19 0.19c-0.01,0.01 -0.01,0.02 -0.01,0.04l0.07 0.22c0,0.01 0.01,0.02 0.02,0.02l0.22 0.07c0.01,0 0.03,0 0.04,-0.01l0.19 -0.19c0,0.01 0,0.01 0,0.02 0,0.2 -0.16,0.37 -0.36,0.37 -0.03,0 -0.06,-0 -0.09,-0.01 -0.01,-0 -0.03,0 -0.03,0.01l0 0 -0.83 0.83zm0.03 -0.71l-0.48 -0.48c-0,-0 -0.01,-0.01 -0.02,-0.01l-0.13 -0.03 -0.12 -0.22 0.11 -0.11 0.22 0.12 0.03 0.13c0,0.01 0,0.01 0.01,0.02l0.48 0.48 0 0 0.05 0.05 -0.09 0.09 -0.05 -0.05zm1.28 0.77c-0,-0 -0,-0 -0,-0l-0.44 -0.44c-0.01,-0.01 -0.04,-0.01 -0.05,0l-0.02 0.02c-0.02,0.02 -0.04,0.02 -0.06,0 -0.02,-0.02 -0.02,-0.04 -0,-0.06 0,-0 0,-0 0,-0l0.02 -0.02c0.01,-0.01 0.01,-0.04 0,-0.05l-0.08 -0.08 0.19 -0.19c0.03,0.01 0.06,0.01 0.09,0.01 0.24,0 0.44,-0.19 0.44,-0.44 0,-0.04 -0,-0.07 -0.01,-0.11 -0,-0.02 -0.02,-0.03 -0.04,-0.03 -0.01,0 -0.01,0 -0.02,0.01l-0.23 0.23 -0.18 -0.06 -0.06 -0.18 0.23 -0.23c0.01,-0.01 0.01,-0.04 -0,-0.05 -0,-0 -0.01,-0.01 -0.02,-0.01 -0.23,-0.06 -0.47,0.08 -0.53,0.32 -0.01,0.04 -0.01,0.07 -0.01,0.11 0,0.03 0,0.06 0.01,0.09l-0.3 0.3 -0.06 -0.06 -0 0 -0.46 -0.46 -0.03 -0.14c-0,-0.01 -0.01,-0.02 -0.02,-0.02l-0.26 -0.15c-0.01,-0.01 -0.03,-0.01 -0.04,0.01l-0.15 0.15c-0.01,0.01 -0.01,0.03 -0.01,0.04l0.15 0.26c0,0.01 0.01,0.01 0.02,0.02l0.14 0.03 0.47 0.47 0.05 0.05 -0.3 0.3c-0.03,-0.01 -0.06,-0.01 -0.09,-0.01 -0.24,0 -0.44,0.2 -0.44,0.44 0,0.24 0.2,0.44 0.44,0.44 0.24,0 0.44,-0.2 0.44,-0.44 -0,-0.03 -0,-0.06 -0.01,-0.09l0.19 -0.19 0.08 0.08c0.01,0.01 0.04,0.01 0.05,0l0.02 -0.02c0.02,-0.02 0.04,-0.02 0.06,0 0.02,0.02 0.02,0.04 0,0.06l-0.02 0.02c-0.01,0.01 -0.01,0.04 0,0.05l0.44 0.44c0.11,0.12 0.3,0.12 0.42,0 0.12,-0.12 0.12,-0.3 0,-0.42z"/>
                                </g>
                        </svg>
                    </div>
                    <h3 class="service-content-mainContent-header">
                        Walk&nbsp;-&nbsp;in&nbsp;service&nbsp;and&nbsp;repairs
                    </h3>
                    <p class="service-content-mainContent-text">
                        If you bought your phone from a shop or an operator,
                        please take it back to the store you bought it
                        from to receive service and repairs.
                    </p>
                    <div class="service-content-mainContent-svgLocation">
                        <!-- <img class="location-svg" src="img/Location.svg" alt=""> -->
                        <svg class="location-svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="0.467008in" height="0.666665in" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 1.08 1.54">

                                <g id="Layer_x0020_1">
                                    <metadata id="CorelCorpID_0Corel-Layer"/>
                                    <path class="supportSvg" d="M0.92 0.88l-0.38 0.55 -0.38 -0.55c-0.15,-0.19 -0.12,-0.51 0.04,-0.68 0.09,-0.09 0.21,-0.14 0.34,-0.14 0.13,0 0.25,0.05 0.34,0.14 0.17,0.17 0.19,0.49 0.04,0.68zm-0 -0.72l0 0c-0.1,-0.1 -0.24,-0.16 -0.38,-0.16 -0.14,0 -0.28,0.06 -0.38,0.16 -0.19,0.19 -0.21,0.54 -0.05,0.76l0.43 0.62 0 0 0.43 -0.62c0.16,-0.22 0.14,-0.57 -0.05,-0.76z"/>
                                    <path class="supportSvg" d="M0.68 0.54c0,0.08 -0.06,0.14 -0.14,0.14 -0.08,0 -0.14,-0.06 -0.14,-0.14 0,-0.08 0.06,-0.14 0.14,-0.14 0.08,0 0.14,0.06 0.14,0.14zm-0.14 -0.2c-0.11,0 -0.2,0.09 -0.2,0.2 0,0.11 0.09,0.2 0.2,0.2 0.11,0 0.2,-0.09 0.2,-0.2 0,-0.11 -0.09,-0.2 -0.2,-0.2z"/>
                                </g>
                        </svg>
                        <a href="#" class="service-content-mainContent-searchLocation">Search&nbsp;Location</a>
                    </div>
                    <!-- <a href="#" class="service-content-mainContent-searchLocation">Search&nbsp;Location</a> -->
                </div>
            </div>
        </section>

        <section class="attitudeSupport">
            <div class="attitudeSupport-backGround" style="background:linear-gradient(180deg ,rgba(255, 255, 255,1),transparent, transparent), url('{{asset('images/23.jpg')}}') 35% 0/cover">
                <div class="attitudeSupport-header">
                        <span class="attitudeSupport-header-attitude">
                            Customer&nbsp;support
                        </span>
                    <span class="attitudeSupport-header-attitude">
                            is&nbsp;not&nbsp;a&nbsp;service&#8218;
                        </span>
                    <span class="attitudeSupport-header-attitude">
                            it’s an
                            <span class="attitude">
                                attitude
                            </span>
                            <span class="dot">
                                &#8901;
                            </span>
                        </span>
                </div>
            </div>
        </section>
    </section>
@endsection


{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
{{--    <title>Document</title>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--    <link rel="stylesheet" href="{{'css/style.css'}}">--}}

{{--</head>--}}
{{--<body>--}}

{{--<script>--}}
{{--    document.querySelector('.support-header-search').addEventListener('click', function(e){--}}
{{--        e.stopPropagation();--}}
{{--        document.querySelector('.support-header-search').style.backgroundColor = '#fff';--}}
{{--        document.querySelector('.support-header-search-list').style.backgroundColor = '#fff';--}}
{{--        document.querySelector('.support-header-searchForm-textInput').style.borderColor = 'red';--}}
{{--        document.querySelector('.support-header-search-list').style.display = 'block';--}}
{{--    });--}}

{{--    document.querySelector('.support-header-search-list').addEventListener('click', function(e){--}}
{{--        e.stopPropagation();--}}
{{--    });--}}

{{--    document.querySelector('#support').addEventListener('click', function(){--}}
{{--        document.querySelector('.support-header-search').style.backgroundColor = 'transparent';--}}
{{--        document.querySelector('.support-header-search-list').style.backgroundColor = 'transparent';--}}
{{--        document.querySelector('.support-header-searchForm-textInput').style.borderColor = '#aaaaaa';--}}
{{--        document.querySelector('.support-header-search-list').style.display = 'none';--}}
{{--    });--}}
{{--</script>--}}
{{--</body>--}}
{{--</html>--}}
