@extends('layout')
@section('content')
    <div id="PopularTopics">
        <div class="PopularTopicsContainer">
            <div class="PopularTitle">
                <h3 class="titlePage">Popular topics</h3>
                <div class="topicsSvg">
                    <img src="svg/Topics.svg" alt="Topics" class="svg">
                </div>
                <div class="textHeader">Walk-in service and repairs</div>
                <div class="textContainer">
                    <p>
                        If you bought your phone from a shop or an opera-<br>
                        tor,please take it back to the store you bought it<br>
                        from to receive service and repairs.
                    </p>
                </div>
            </div>
            <div class="container AllCollapses">
                <div id="accordion" class="accordion">
                    <div class="card mb-0 firstCollapse">
                        <div class="card-header collapsed" data-toggle="collapse" data-target="#collapseOne"
                             aria-expanded="false" aria-controls="collapseOne">
                            <a class="card-title">
                                Why does my phone occasionally stop charging before it reaches 100%?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <a class="card-title">
                                My phone won’t charge. What should I do?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <a class="card-title">
                                I recently updated my Dox and am now having some issues with the Camera.
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <a class="card-title">
                                How can I resolve this?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseFour" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            <a class="card-title">
                                How can I close an app that has stopped working?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseFive" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            <a class="card-title">
                                Where can I find the IMEI number for my phone?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseSix" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            <a class="card-title">
                                Can I remove the back cover or battery from my Dox B100?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseSeven" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            <a class="card-title">
                                Where can I get support for my Dox smartphone?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseEight" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                            <a class="card-title">
                                Why does my phone feel warm when it's charging or when I'm using it
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseNine" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                            <a class="card-title">
                                Why can’t I turn off the camera shutter sound?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseTen" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                    <div class="card mb-0 lastCollapse">
                        <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion"
                             data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                            <a class="card-title">
                                Will my Dox smartphone receive security updates?
                            </a>
                            <img src="{{asset('svg/Add.svg')}}" alt="" class="svg add">
                            <img src="{{asset('svg/delete.svg')}}" alt="" class="svg delete">
                        </div>
                        <div id="collapseEleven" class="collapse" data-parent="#accordion">
                            <div class="card-body">
                                Determine if the problem is with the phone or the battery by replacing the battery.
                                Make sure the volume is turned up, the ringer turned on, or the battery is charged.
                                If these strategies don't work, take your cell phone to a service center for replacement
                                or
                                repair. Sometimes it's best to purchase a new phone, because repair costs can be as high
                                as the cost of a new phone.
                                Note: If your phone is recharged through Information Technology Services (ITS), contact
                                ITS to order a replacement phone. Retail stores will generally not assist you with
                                devices
                                that are billed on a business or corporate account.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection