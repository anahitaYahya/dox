@extends('layout')
@section('content')

<div id="firstPage">
    {{--        StartSection 1     --}}
        <div class="row video mb-5">
            <div class="videoText">Ultimate view.</div>
            <a class="videoLink" href="#">Discover it ></a>
        </div>
        {{--        EndSection 1     --}}

        {{--        StartSection 2     --}}
        <div class="row seplex">
            <div class="seplexWaraper col-11 mt-5">
                <div class="seplexContent" style=" background: url('{{asset('images/newSeplex.png')}}')">
                </div>
                <div class="seplexLogo">
                    <img class="svg seplexPic" src="{{asset('svg/SeplexLogo.svg')}}" alt="seplex">
                    <a class="seplexLink" href="#">Discover it ></a>
                </div>
            </div>
        </div>
        {{--        EndSection 2     --}}

        {{--        StartSection 3     --}}
        <div class="row sellexBotlex">
            <div class="sellexWaraper col-11 mt-5 p-0 mb-5">
                <div class="sellex">
                    <div class="sellexContent" style=" background: url('{{asset('images/newSellex_2.png')}}')">
                    </div>
                    <div class="sellexLogo">
                        <img class="svg sellexPic" src="{{asset('svg/SellexLogo.svg')}}" alt="sellex">
                        <a class="sellexLink" href="#">Discover it ></a>
                    </div>
                </div>
                <div class="botlex">
                    <div class="botlexContent" style=" background: url('{{asset('images/newBotlex.png')}}')">
                    </div>
                    <div class="botlexLogo">
                        <img class="svg botlexPic" src="{{asset('svg/Botlex.svg')}}" alt="botlex">
                        <a class="botlexLink" href="#">Discover it ></a>
                    </div>
                </div>
            </div>
        </div>
        {{--        EndSection 3     --}}

        {{--        StartSection 4     --}}
        <div class="row differences">
            <div class="differencesWaraper col-12 mt-5">
                <h2 class="differencesTitle col-12">
                    Here’s a quick look at the differences.
                </h2>
                <div class="col-10 differencesContent">
                    <div class="d-seplex">
                        <div class="content">
                            <div class="d-seplex-image">
                                <img src="{{asset('images/Seplex-1.png')}}" alt="seplex">
                            </div>
                            <div class="d-seplex-title">
                                <h2>Seplex</h2>
                                <div class="color-gray">Starting at $449</div>
                            </div>
                            <div class="desc color-gray mt-3">
                                All-new triple-camera system
                                (Ultra Wide, Wide, Telephoto)
                            </div>
                            <div class="desc color-gray mt-3">
                                Up to 20 hours of video playback1
                            </div>
                            <div class="desc color-gray mt-3">
                                Water resistant to a depth of 4
                                meters for up to 30 minutes2
                            </div>
                            <div class="desc color-gray mt-3 mb-4">
                                5.8” or 6.5” Super Retina XDR
                                display3
                            </div>
                            <div class="desc color-gray mt-4">
                                <a href="#">Learn more ></a>
                            </div>
                        </div>
                    </div>
                    <div class="d-sellex ml-3">
                        <div class="content">
                            <div class="d-seplex-image">
                                <img src="{{asset('images/Sellex_1.png')}}" alt="seplex">
                            </div>
                            <div class="d-seplex-title">
                                <h2>Sellex</h2>
                                <div class="color-gray">Starting at $449</div>
                            </div>
                            <div class="desc color-gray mt-3">
                                All-new dual-camera system
                                (Ultra Wide, Wide)
                            </div>
                            <div class="desc color-gray mt-3">
                                Up to 17 hours of video playback1
                            </div>
                            <div class="desc color-gray mt-3">
                                Water resistant to a depth of 2
                                meters for up to 30 minutes2
                            </div>
                            <div class="desc color-gray mt-3 mb-4">
                                6.1” Liquid Retina HD display3
                            </div>
                            <div class="desc color-gray mt-4">
                                <a href="#">Learn more ></a>
                            </div>
                        </div>
                    </div>
                    <div class="d-botlex ml-3">
                        <div class="content">
                            <div class="d-seplex-image">
                                <img src="{{asset('images/Botlex-1.png')}}" alt="seplex">
                            </div>
                            <div class="d-seplex-title">
                                <h2>Botlex2</h2>
                                <div class="color-gray">Starting at $449</div>
                            </div>
                            <div class="desc color-gray mt-3">
                                Single-camera system
                                (Wide)
                            </div>
                            <div class="desc color-gray mt-3">
                                Up to 16 hours of video playback1
                            </div>
                            <div class="desc color-gray mt-3">
                                Water resistant to a depth of 1
                                meter for up to 30 minutes2
                            </div>
                            <div class="desc color-gray mt-3 mb-4">
                                6.1” Liquid Retina HD display3
                            </div>
                            <div class="desc color-gray mt-4">
                                <a href="#">Learn more ></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        EndSection 4     --}}

        {{--        StartSection 5     --}}
        <div class="row warranty">
            <div class="warrantyWaraper col-10 mt-5 mb-5">
                <div class="warrantyContent">
                    <div class="warrantySection">
                        <img class="svg" src="{{asset('svg/Warranty.svg')}}" alt="Warranty">
                        <h3 class="w-title">Warranty</h3>
                        <div class="w-desc">If you have questions</div>
                        <div class="w-desc">about warranties on new or</div>
                        <div class="w-desc">refurbished hardware, just select a</div>
                        <div class="w-desc">product below.</div>
                        <a class="mt-3" href="#">Learn more ></a>
                    </div>
                    <div class="SparepartSection">
                        <img class="svg" src="{{asset('svg/Sparepart.svg')}}" alt="Sparepart">
                        <h3 class="w-title">Spare part</h3>
                        <div class="w-desc">If you have questions</div>
                        <div class="w-desc">about warranties on new or</div>
                        <div class="w-desc">refurbished hardware, just select a</div>
                        <div class="w-desc">product below.</div>
                        <a class="mt-3" href="#">Learn more ></a>
                    </div>
                </div>
            </div>
        </div>
        {{--        EndSection 5     --}}

        {{--        StartSection 6     --}}
        <div class="row feature">
            <div class="featureWaraper col-12">
                <div class="featurePhone col-11">
                    About Feature Phone<br>
                    with<br>
                    Page Link
                </div>
                <div class="magnetic col-11 mt-5 p-0 mb-5">
                    <div class="magneticPic" style=" background: url('{{asset('images/headPhoneCable.png')}}')">
                        <div class="m-p-content">
                            <div class="m-title-up">MAGNETIC</div>
                            <div class="m-title">USB CABLE</div>
                            <div class="m-title-down">Charging and data Transfer</div>
                            <a class="mt-2" href="#">Learn more ></a>
                        </div>
                    </div>
                    <div class="headphone" style=" background: url('{{asset('images/headPhone.png')}}')">

                    </div>
                </div>
            </div>
        </div>
        {{--        EndSection 6     --}}

        {{--        StartSection 7     --}}
        <div class="row camera">
            <div class="cameraWaraper col-12" style=" background: url('{{asset('images/camera.png')}}')">
                <div class="cameraText" >
                    <span class="t-w">A </span><span>camera </span><span class="t-w">puts</span><br>
                    <span class="t-w">the whole </span><span>world </span><br>
                    <span class="t-w">on </span><span>your hands</span><spanclass="t-w">.</span><br>
                </div>
            </div>
        </div>
        {{--        EndSection 7     --}}
    </div>
@endsection
